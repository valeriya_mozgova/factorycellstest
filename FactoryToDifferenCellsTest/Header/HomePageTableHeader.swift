//
//  HomePageTableHeader.swift
//  Enguide
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class HomePageTableHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: Public
    public func configureHeaderWithTitle(title: String) {
        self.titleLabel.text = title
        self.backgroundView = UIView(frame: self.frame)
        self.backgroundView?.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        
    }
    
    //MARK: Actions
    @IBAction func didClickViewAll(_ sender: UIButton) {
        
    }
}
