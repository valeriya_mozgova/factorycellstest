//
//  Model.swift
//  FactoryToDifferenCellsTest
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import Foundation

struct Section {
    
    var sectionTitle: String

    var displayModels: [BaseDisplayModel]
    
}

class Trip: BaseDisplayModel {
    
    var tripDescrition: String
    var title: String
    var picture: String
    
    init(title: String, picture: String, description: String) {
        
        self.tripDescrition = description
        self.title = title
        self.picture = picture
        
        super.init(cellType: .tripCell)
    }
}


class Place: BaseDisplayModel {
    
    var title: String
    var picture: String
    
    init(title: String, picture: String) {
        
        self.title = title
        self.picture = picture
        
        super.init(cellType: .placeCell)
    }
}

class People: BaseDisplayModel {
    var people: [Person]
    
    init(people: [Person]) {
        self.people = people
        
        super.init(cellType: .peopleListCell)
    }
}

struct Person {
    
    var name:  String
    var avatar: String
    var tripCount: Int
}


class BaseDisplayModel {
    
    var cellId: CellType

    init(cellType: CellType) {

        self.cellId = cellType
    }
}


enum CellType: String {
    case tripCell = "TripCellID"
    case peopleListCell = "PeopleCellID"
    case placeCell = "PlaceCellID"
}
