//
//  Helper.swift
//  FactoryToDifferenCellsTest
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import UIKit

class Helper: NSObject {

   static func createContent() -> [Section] {
    
    var content = [Section]()
    
    let latestTrips = [Trip(title: "Trip to Africa", picture: "avatar_bear", description: "lololololol"), Trip(title: "Trip to India", picture: "avatar_lion", description: "azazazaz")]
    
    let cityTrips = [Trip(title: "Trip to London", picture: "avatar_wolf", description: "lololololol"), Trip(title: "Trip to Amster", picture: "avatar_fox",description: "lololololol")]
    
    let peopleArr = [Person(name: "Alexey Ivanov", avatar: "avatar_raccoon", tripCount: 10), Person(name: "Alexey Arvanidi", avatar: "avatar_tiger", tripCount: 11),Person(name: "", avatar: "", tripCount: 23), Person(name: "", avatar: "", tripCount: 77)]
    
    let people =  People(people: peopleArr)
    
    let newPlaces = [Place(title: "Drama theatre", picture: "avatar_fox")]
    let recommendedPlaces = [Place(title: "Attend", picture: "avatar_ferret"), Place(title: "Attend2", picture: "avatar_weasel")]
    
    let section1 = Section(sectionTitle: "Latest Trips", displayModels: latestTrips)
    let section3 = Section(sectionTitle: "New Places", displayModels: newPlaces)
    let section4 = Section(sectionTitle: "People", displayModels: [people])
    let section2 = Section(sectionTitle: "City Trips", displayModels: cityTrips)
    let section5 = Section(sectionTitle: "Recommended Places", displayModels: recommendedPlaces)
   
    content.append(section1)
    content.append(section2)
    content.append(section3)
    content.append(section4)
    content.append(section5)
    return content

    }
}
