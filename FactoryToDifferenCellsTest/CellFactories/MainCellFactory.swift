//
//  MainCellFactory.swift
//  FactoryToDifferenCellsTest
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import Foundation
import UIKit

class MainCellFactory {
    
    func createCell(forTable tableView: UITableView, withContent content: BaseDisplayModel) -> UITableViewCell {
        
        switch content.cellId {
        case .tripCell:
            
            let  cell = tableView.dequeueReusableCell(withIdentifier: content.cellId.rawValue) as! TripCell
            
            cell.configure(withTrip: content as! Trip)
            
            return cell

        case .peopleListCell:
            
           let cell = tableView.dequeueReusableCell(withIdentifier: content.cellId.rawValue) as! PeopleCell
           
           let peopleList = content as! People
        
            cell.configure(content: peopleList.people)
           
           return cell
            
        case .placeCell:
            
          let  cell = tableView.dequeueReusableCell(withIdentifier: content.cellId.rawValue) as! PlaceCell
          
          cell.configure(withPlace: content as! Place)
            return cell
        }
    }
}
