//
//  ViewController.swift
//  FactoryToDifferenCellsTest
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var content = [Section]()
    
    var cellFactory = MainCellFactory()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registeXibs()
        
        self.content = Helper.createContent()
    }

    func registeXibs() {
        
        let tripCell = UINib(nibName: "TripCell", bundle: nil)
        tableView.register(tripCell, forCellReuseIdentifier: CellType.tripCell.rawValue)
        
        let placeCell = UINib(nibName: "PlaceCell", bundle: nil)
        tableView.register(placeCell, forCellReuseIdentifier: CellType.placeCell.rawValue)
        
        let peopleCell = UINib(nibName: "PeopleCell", bundle: nil)
        tableView.register(peopleCell, forCellReuseIdentifier: CellType.peopleListCell.rawValue)
        
        let nib = UINib(nibName: "Header", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
    }
}


extension ViewController: UITableViewDelegate {
 

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 260
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! HomePageTableHeader

        
        header.configureHeaderWithTitle(title: content[section].sectionTitle)
        
        return header;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 58
    }
}


extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cellFactory.createCell(forTable: tableView, withContent: content[indexPath.section].displayModels[indexPath.row])
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return self.content.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       let section = content[section]
        
       return section.displayModels.count
    }
}






