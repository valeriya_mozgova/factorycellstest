//
//  PlaceCell.swift
//  FactoryToDifferenCellsTest
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {

    
    @IBOutlet weak var pictureImgView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func configure(withPlace place: Place) {
        self.pictureImgView.image = UIImage(named: place.picture)
        self.titleLabel.text = place.title
    }
    
}
