//
//  PeopleCell.swift
//  FactoryToDifferenCellsTest
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import UIKit

class PeopleCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nib = UINib(nibName: "PersonCollectionCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "PersonCollectionCellID")
    }

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
        }
    }
    
    var content = [Person]()
    
    public func configure(content: [Person]) {
        self.content = content
    }
}


extension PeopleCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return content.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonCollectionCellID", for: indexPath) as! PersonCollectionCell
        
        cell.configure(withPerson: content[indexPath.row])
        
        return cell
    }
    
}
