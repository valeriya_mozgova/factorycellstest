//
//  TripCell.swift
//  FactoryToDifferenCellsTest
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {


    @IBOutlet weak var pictureImgView: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
   public func configure(withTrip trip: Trip) {
        self.pictureImgView.image = UIImage(named: trip.picture)
        self.title.text = trip.title
    
    self.descriptionLabel.text = trip.tripDescrition
    }
}
