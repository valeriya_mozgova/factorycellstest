//
//  PersonCollectionCell.swift
//  Enguide
//
//  Created by Lera Mozgovaya on 1/29/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit


class PersonCollectionCell: UICollectionViewCell {

    
    @IBOutlet weak var avatarImgView: UIImageView!
    
    @IBOutlet weak var tripsCountLabel: UILabel!
    
    @IBOutlet weak var personNameLabel: UILabel!
    
    //MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK: Actions
    @IBAction func didClickFollow(_ sender: UIButton) {
        
    }

    public func configure(withPerson person: Person) {
        
        self.avatarImgView.image = UIImage.init(named: person.avatar)
        self.tripsCountLabel.text = "\(person.tripCount)"
        self.personNameLabel.text = person.name
    }
    
}


